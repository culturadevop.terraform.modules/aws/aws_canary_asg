output "alb_endpoint" {
  description = "Public DNS of ALB"
  value       = aws_lb.alb.dns_name
}

output "debug" {
  description = "For debug purpose."
  value       = var.resources
}
